package com.joucestudio.quakes.app;

import android.app.Application;

import com.joucestudio.quakes.rest.client.RestClient;

public class App extends Application
{
    private static RestClient restClient;

    @Override
    public void onCreate()
    {
        super.onCreate();
        restClient = new RestClient();
    }

    /*
    1. Returns the RestClient from any activity - Global scope
    2. Used as a part of Retrofit call
     */
    public static RestClient getRestClient()
    {
        return restClient;
    }
}
