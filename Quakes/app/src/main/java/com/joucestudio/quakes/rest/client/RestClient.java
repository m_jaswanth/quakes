package com.joucestudio.quakes.rest.client;

import com.google.gson.GsonBuilder;
import com.joucestudio.quakes.rest.service.EarthQuakeService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient
{
    private static final String BASE_URL = "http://www.seismi.org/";
    private EarthQuakeService apiService;

    public RestClient()
    {

        /*
        1. Single instance builder
        2. Used to create link between app and API server
         */
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();

        apiService = retrofit.create(EarthQuakeService.class);
    }

    public EarthQuakeService getEarthQuakeService()
    {
        return apiService;
    }
}
