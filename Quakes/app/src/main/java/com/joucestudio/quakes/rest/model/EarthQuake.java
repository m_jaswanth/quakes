package com.joucestudio.quakes.rest.model;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

/*
        1. EarthQuake class generated from jsonschema2pojo.org
        2. Used as prime model for data retrieved from API
         */

@Generated("org.jsonschema2pojo")
public class EarthQuake {

    private String src;
    private String eqid;
    private String timedate;
    private String lat;
    private String lon;
    private String magnitude;
    private String depth;
    private String region;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The src
     */
    public String getSrc() {
        return src;
    }

    /**
     *
     * @param src
     * The src
     */
    public void setSrc(String src) {
        this.src = src;
    }

    /**
     *
     * @return
     * The eqid
     */
    public String getEqid() {
        return eqid;
    }

    /**
     *
     * @param eqid
     * The eqid
     */
    public void setEqid(String eqid) {
        this.eqid = eqid;
    }

    /**
     *
     * @return
     * The timedate
     */
    public String getTimedate() {
        return timedate;
    }

    /**
     *
     * @param timedate
     * The timedate
     */
    public void setTimedate(String timedate) {
        this.timedate = timedate;
    }

    /**
     *
     * @return
     * The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The lon
     */
    public String getLon() {
        return lon;
    }

    /**
     *
     * @param lon
     * The lon
     */
    public void setLon(String lon) {
        this.lon = lon;
    }

    /**
     *
     * @return
     * The magnitude
     */
    public String getMagnitude() {
        return magnitude;
    }

    /**
     *
     * @param magnitude
     * The magnitude
     */
    public void setMagnitude(String magnitude) {
        this.magnitude = magnitude;
    }

    /**
     *
     * @return
     * The depth
     */
    public String getDepth() {
        return depth;
    }

    /**
     *
     * @param depth
     * The depth
     */
    public void setDepth(String depth) {
        this.depth = depth;
    }

    /**
     *
     * @return
     * The region
     */
    public String getRegion() {
        return region;
    }

    /**
     *
     * @param region
     * The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}