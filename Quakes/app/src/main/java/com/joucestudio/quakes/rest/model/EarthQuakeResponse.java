package com.joucestudio.quakes.rest.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class EarthQuakeResponse {

    /*
        1. Response wrapper class from API call
        2. Contains count and array of EarthQuake instances
         */

    private String count;
    private List<EarthQuake> earthquakes = new ArrayList<EarthQuake>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The count
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The earthquakes
     */
    public List<EarthQuake> getEarthquakes() {
        return earthquakes;
    }

    /**
     *
     * @param earthquakes
     * The earthquakes
     */
    public void setEarthquakes(List<EarthQuake> earthquakes) {
        this.earthquakes = earthquakes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}