package com.joucestudio.quakes.rest.service;

import com.joucestudio.quakes.rest.model.EarthQuakeResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EarthQuakeService
{
    /*
    1. GET method RESTful API call
    2. End point - /api/eqs
    3. No additional paramenters
     */
    @GET("/api/eqs")
    Call<EarthQuakeResponse> getEarthquakes();
}
