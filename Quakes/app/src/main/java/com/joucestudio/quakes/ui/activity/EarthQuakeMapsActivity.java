package com.joucestudio.quakes.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.joucestudio.quakes.R;
import com.joucestudio.quakes.app.App;
import com.joucestudio.quakes.rest.model.EarthQuake;
import com.joucestudio.quakes.rest.model.EarthQuakeResponse;
import com.joucestudio.quakes.ui.maps.ClusterMarkerLocation;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarthQuakeMapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private int ZOOM_LEVEL = 5;
    private GoogleMap mMap;

    private ClusterManager<ClusterMarkerLocation> mClusterManager;
    private ArrayList<EarthQuake> earthQuakes;

    private Toolbar toolbar;
    private Button showList;
    private BottomSheetLayout bottomSheet;

    private RecyclerView recyclerView;
    private EarthQuakeRVAdapter rvAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earth_quake_maps);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //Button to show bottom sheet
        showList = (Button) findViewById(R.id.showList);
        showList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //bottomsheet instance is created and a recycler view is configured
                final BottomSheetLayout bottomSheet = (BottomSheetLayout) findViewById(R.id.bottomsheet);
                bottomSheet.showWithSheetView(LayoutInflater.from(getApplicationContext()).inflate(R.layout.activity_main, bottomSheet, false));

                recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
                rvAdapter = new EarthQuakeRVAdapter(earthQuakes);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(rvAdapter);

                recyclerView.setVisibility(View.VISIBLE);

            }
        });

        bottomSheet = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        bottomSheet.showWithSheetView(LayoutInflater.from(getApplicationContext()).inflate(R.layout.activity_main, bottomSheet, false));
        bottomSheet.setShouldDimContentView(true);
        bottomSheet.setPeekOnDismiss(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Retrofit asynchronous call which consumes earthquake list
        Call<EarthQuakeResponse> call = App.getRestClient().getEarthQuakeService().getEarthquakes();
        call.enqueue(new Callback<EarthQuakeResponse>() {
            @Override
            public void onResponse(Call<EarthQuakeResponse> call, Response<EarthQuakeResponse> response) {
                Log.i("API", "Success");


                if (response.body() != null) {

                    //consume list of earthquakes and assign to earthquakes arraylist
                    earthQuakes = (ArrayList<EarthQuake>) response.body().getEarthquakes();

                    //configure recycler view adapter and link it to earthquakes arraylist
                    rvAdapter = new EarthQuakeRVAdapter(earthQuakes);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(rvAdapter);

                    //show markers
                    if (response.body() != null) {
                        if (response.body().getEarthquakes().size() > 0) {
                            setUpClusterer();
                            refreshList();
                        }
                    }

                    //start with first latlng in the list
                    animateToLatlng(new LatLng(Double.parseDouble(earthQuakes.get(0).getLat()),
                            Double.parseDouble(earthQuakes.get(0).getLon())));

                    //show current earthquake data in toolbar
                    toolbar.setTitle(earthQuakes.get(0).getRegion());
                    Log.e("Text:" , toolbar.getTitle() + "");
                }

            }

            @Override
            public void onFailure(Call<EarthQuakeResponse> call, Throwable t) {
                Log.e("API", "Failure");
            }
        });
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        //no need - all markers are now changed into ClusterItem
        return false;
    }

    private void refreshList()
    {
        //create a marker for each earthquake
        for (int i = 0; i < earthQuakes.size(); i++) {
            LatLng latLng = new LatLng(Double.parseDouble(earthQuakes.get(i).getLat()),
                    Double.parseDouble(earthQuakes.get(i).getLon()));

            //add marker to cluster
            ClusterMarkerLocation marker = new ClusterMarkerLocation(latLng);
            marker.setIndex(i);
            mClusterManager.addItem(marker);
        }
    }

    private void setUpClusterer() {

        //initialize cluster manager
        mClusterManager = new ClusterManager<>(this, mMap);

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);

        //set onclick listener for marker. Show data in toolbar
        mClusterManager
                .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ClusterMarkerLocation>() {
                    @Override
                    public boolean onClusterItemClick(ClusterMarkerLocation item) {

                        toolbar.setTitle(earthQuakes.get(item.getIndex()).getRegion());
                        return false;
                    }
                });
    }

    /*
    Animate map camera to given latlng
     */
    private void animateToLatlng(LatLng latLng) {
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                latLng, ZOOM_LEVEL);
        mMap.animateCamera(location);
    }

    /*
    vibrate differently based on magnitude
     */
    private void vibrate(String magnitude) {
        float duration = Float.parseFloat(magnitude) * 100;
        Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate((long) duration);
    }

    private boolean isVisible(LatLng latLng) {
        if (mMap != null) {
            //This is the current user-viewable region of the map
            LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

            if (bounds.contains(latLng)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    //Recycler view Adapter
    public class EarthQuakeRVAdapter extends RecyclerView.Adapter<EarthQuakeRVAdapter.MyViewHolder> {

        private ArrayList<EarthQuake> earthQuakes;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView depth, region, magnitude;

            public MyViewHolder(View view) {
                super(view);
                depth = (TextView) view.findViewById(R.id.depth);
                region = (TextView) view.findViewById(R.id.region);
                magnitude = (TextView) view.findViewById(R.id.magnitude);
            }
        }


        public EarthQuakeRVAdapter(ArrayList<EarthQuake> earthQuakes) {
            this.earthQuakes = earthQuakes;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_earth_quake_recycler_view, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final EarthQuake earthQuake = earthQuakes.get(position);
            holder.depth.setText("Depth : " + earthQuake.getDepth());
            holder.magnitude.setText(earthQuake.getMagnitude());
            holder.region.setText(earthQuake.getRegion());


            //set onclick listener for all each item

            holder.region.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animateToLatlng(new LatLng(Double.parseDouble(earthQuake.getLat()),
                            Double.parseDouble(earthQuake.getLon())));
                    toolbar.setTitle(earthQuake.getRegion());
                    bottomSheet.dismissSheet();
                }
            });

            holder.depth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animateToLatlng(new LatLng(Double.parseDouble(earthQuake.getLat()),
                            Double.parseDouble(earthQuake.getLon())));
                    toolbar.setTitle(earthQuake.getRegion());
                    bottomSheet.dismissSheet();
                }
            });

            //vibrate when user clicks on magnitude
            holder.magnitude.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vibrate(earthQuake.getMagnitude());
                }
            });
        }

        @Override
        public int getItemCount() {
            return earthQuakes.size();
        }
    }

}
