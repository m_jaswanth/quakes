package com.joucestudio.quakes.ui.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;


/*
1. Custom ClusterMarkerLocation
2. Also holds index which connects each marker to arraylist of earthquakes
 */

public class ClusterMarkerLocation implements ClusterItem {

    private LatLng position;
    private int index;

    public ClusterMarkerLocation(LatLng latLng ) {
        position = latLng;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    public void setPosition( LatLng position ) {
        this.position = position;
    }


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}